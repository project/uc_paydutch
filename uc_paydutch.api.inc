<?php
/**
 * @file
 * File providing wrapper functions to the Paydutch API
 */

/**
 * Request the available payment methods
 */
function uc_paydutch_api_request_payment_methods() {
  $username = variable_get('uc_paydutch_merchant_login', '');
  $password = variable_get('uc_paydutch_merchant_pass', '');

  $xmldata = <<<FILE
  <?xml version="1.0" encoding="UTF-8"?>
    <request>
      <type>listmethod</type>
      <merchant>
        <username>{$username}</username>
        <password>{$password}</password>
      </merchant>
    </request>
FILE;
  $response = _uc_paydutch_make_api_call($xmldata);
  $methods = _uc_paydutch_api_parse_method_response($response);
  if (!is_array($methods)) {
    return array();
  }
  return $methods;
}

/**
 * Parse the response file received after requesting the available payment mehods
 */
function _uc_paydutch_api_parse_method_response($xml) {
  if (drupal_substr($xml, 0, 5) != "<?xml") {
    return array();
  }
  $result = array();
  $xmlobj = simplexml_load_string($xml);
  if (is_object($xmlobj)) {
    $methods = $xmlobj->xpath("//method");
    foreach ($methods as $method) {
      $code = (string) $method->methodcode;
      $name = (string) $method->methodname;
      $result[$code] = $name;
    }
    return $result;
  }
}

/**
 * Perform a payment through the Paydutch system
 */
function uc_paydutch_api_request_payment($desc, $ref, $amount, $payment_method) {
$username = variable_get('uc_paydutch_merchant_login', '');
$password = variable_get('uc_paydutch_merchant_pass', '');
$callbackurl = url('cart/complete/'. $ref, array('absolute' => TRUE));
$xmldata = <<<FILE
  <?xml version=\"1.0\" encoding=\"UTF-8\"?>
  <request>
    <type>transaction</type>
    <transactionreq>
      <description>{$desc}</description>
      <reference>{$ref}</reference>
      <amount>{$amount}</amount>
      <needconsumeraddress>false</needconsumeraddress>
      <username>{$username}</username>
      <password>{$password}</password>
      <callbackurl>{$callbackurl}</callbackurl>
      <paymentmethod>{$payment_method}</paymentmethod>
    </transactionreq>
  </request>
FILE;
  $result = _uc_paydutch_make_api_call($xmldata);
  return $result;
}

/**
 * Request the payment state of a certain order
 */
function uc_paydutch_request_payment_state($orderid) {
$username = variable_get('uc_paydutch_merchant_login', '');
$password = variable_get('uc_paydutch_merchant_pass', '');
$xmldata = <<<FILE
  <?xml version=\"1.0\" encoding=\"UTF-8\"?>
  <request>
    <type>query</type>
    <merchant>
      <reference>{$orderid}</reference>
      <username>{$username}</username>
      <password>{$password}</password>
    </merchant>
  </request>
FILE;
  $result = _uc_paydutch_make_api_call($xmldata);
  return $result;
}

/**
 * Parse the response file received after requesting a payment state
 */
function uc_paydutch_parse_payment_state($xml) {
  if (drupal_substr($xml, 0, 5) != "<?xml") {
    return NULL;
  }
  $xmlobj = simplexml_load_string($xml);
  if (is_object($xmlobj)) {
    $state = $xmlobj->xpath("//state");
    return (string) $state[0];
  }
}


/**
 * Helper function to process the XML requests to Paydutch
 */
function _uc_paydutch_make_api_call($xml) {
  $ch = curl_init();
  $res = curl_setopt($ch, CURLOPT_URL, "https://www.paydutch.nl/api/processreq.aspx");
  $header[] = "Content-type: text/xml";
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}
