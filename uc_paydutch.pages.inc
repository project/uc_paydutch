<?php

/**
 * @file
 * Provides landing pages
 */

/**
 * Render the payment complete page
 */
function uc_paydutch_complete() {
  if (is_numeric($_GET['Reference'])) {
    $orderid = $_GET['Reference'];
    $state = $_GET['PaymentState'];
    if ($orderid > 0 && $state == "Success") {
      $output = _uc_paydutch_finalize_order($orderid);
    }
  }
  else {
    $orderid = $_SESSION['cart_order'];
    if ($orderid > 0) {
      $order = uc_order_load($orderid);
      $response = uc_paydutch_request_payment_state($order->order_id);
      $state = uc_paydutch_parse_payment_state($response);
      if ($state == "Success") {
        $output = _uc_paydutch_finalize_order($orderid);
      }
    }
  }
  $page = variable_get('uc_cart_checkout_complete_page', '');
  if (!empty($page)) {
    drupal_goto($page);
  }
  else {
    return $output;
  }
}

/**
 * Finalize the order after payment was successful
 */
function _uc_paydutch_finalize_order($orderid) {
  if ($orderid > 0) {
    $order = uc_order_load($orderid);
    uc_order_comment_save($order->order_id, 0, t('Payment received through Paydutch.'), 'admin');
    $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
  }
  return $output;
}

/**
 * Show an error page if payment failed or was cancelled.
 */
function uc_paydutch_error() {
  return theme('uc_paydutch_error');
}

function theme_uc_paydutch_error() {
  $output .= '<p>&nbsp;</p><p>'. t('A problem occurred processing your payment.') .'</p>';
  $output .= l(t('Click here to go back to the checkout page'), 'cart/checkout');
  return $output;
}