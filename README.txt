
Description
-----------
This module integrates the PayDutchGroup / WeDeal payment gateway with Ubercart.
For more information about this service: http://www.paydutchgroup.nl/ - http://www.wedeal.nl

Installation
------------

1) Follow the normal steps to install & activate this module
   See http://www.ubercart.org/docs/user/10972/installing_contributed_modules for more info
2) Go to Administer > Store administration > Configuration > Payment settings > Payment Gateways
3) In the fieldset 'Pay Dutch settings', enter your PayDutch Group merchant username & password 
4) Go to Administer > Store administration > Configuration > Payment settings > Payment Methods
   The payment methods that have been activated for your PayDutch Group account should be listed here. 
   Enable the ones you wish to use.
5) Go to the PayDutch Manager website and browse to the "Technical settings" page.
6) Enter the following details:
  * Success CallbackURL: http://<yourdomain>/cart/paydutch/complete
  * Fail CallbackURL: http://<yourdomain>/cart/paydutch/error
  
Sponsor
-------
Development of this module was sponsored by Pure Sign (http://www.puresign.be)

Author
------
Sven Decabooter (http://drupal.org/user/35369)
The author can be contacted for paid customizations of this module as well as Drupal consulting and development.
  